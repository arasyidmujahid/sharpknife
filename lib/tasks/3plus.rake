require 'open-uri'

namespace :tiga_plus do

  task :import do

    _3plus_working_dir = '/Users/rasyidmujahid/Repo/sellers/3plus/'
    _3plus_product_info_csv = 'lampu.csv'

    product_details = {}

    CSV.foreach(Rails.root.join(_3plus_working_dir, _3plus_product_info_csv), { headers: :true }) do |row|
      details = {
        name: row[1],
        description: row[2],
        price: row[3],
        image_filename: row[6],
      }
      product_id = row[1]
      product_details[product_id] = details
    end

    uploaded_products = CSV.foreach(Rails.root.join('data', '3plus', 'uploaded_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome, :prefs => prefs, :desired_capabilities => caps)

    # driver.manage.timeouts.implicit_wait = 300
    driver.manage.window.maximize

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "shop@grya.co.id"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "gryashop2016"
    # driver.find_element(:id, "send2").click
    save_btn = driver.find_element(:id, "send2")
    driver.action.move_to(save_btn).click(save_btn).perform
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    # image_files = Dir.glob(Rails.root.join(_3plus_working_dir + '/images/*'))

    limit = 1

    product_details.each do |product_id, details|

      next if uploaded_products.include?(product_id)

      # product official name
      _name = "#{details[:name]}"
      p "_name: #{_name}"

      # description
      _description = details[:description].gsub('\n', '<br>').split(';').join("</li><li>")
      _description = "<ul><li>#{_description}</li></ul>"
      p "_description: #{_description}"

      # sku
      _sku = "3plus-#{details[:name].gsub(' ', '-').downcase}"
      if _sku.length > 64
        how_many = _sku.length - 64
        _sku = "3plus-#{details[:name][0..how_many].gsub(' ', '-').downcase}"
      end
      p "_sku #{_sku}"

      # price
      _price = details[:price].gsub(/\D/, '').to_i
      p "_price #{_price}"

      # stock
      _stock = 2

      # details
      # if details[:details]
      #   _details = details[:details].split(';').join("</li><li>") 
      #   _details = "<ul><li>#{_details}</li></ul>"
      #   p "_details #{_details}"
      # end
      
      # _finishing = details[:finishing]
      # _color = details[:color]
      # _details = "<ul><li>Finishing: #{_finishing}</li><li>Color: #{_color}</li></ul>"

      # _materials = details[:materials].split("\r").join("</li><li>")
      # _materials = "<ul><li>#{_materials}</li></ul>"

      # _dimensions = details[:dimensions] if details[:dimensions]

      driver.find_element(:id, "save_butn").click

      wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      wait.until do
        driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div/div[2]/div[2]/div/div[3]/div[1]").size > 0
      end

      # sleep(3)
      # open category
      driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[3]/span[1]").click
      sleep(2)

      driver.find_element(:xpath, "//input[@name='category[]'][@value='10']").click
      driver.find_element(:xpath, "//input[@name='category[]'][@value='106']").click

      driver.find_element(:id, "name").clear
      driver.find_element(:id, "name").send_keys "#{_name}"
      driver.find_element(:id, "description").clear
      driver.find_element(:id, "description").send_keys "#{_description}"
      driver.find_element(:id, "descriptions").clear
      driver.find_element(:id, "descriptions").send_keys "#{_description}"
      driver.find_element(:id, "sku").clear
      driver.find_element(:id, "sku").send_keys "#{_sku}"
      driver.find_element(:id, "price").clear
      driver.find_element(:id, "price").send_keys "#{_price}"
      driver.find_element(:id, "stock").clear
      driver.find_element(:id, "stock").send_keys "#{_stock}"
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
      driver.find_element(:id, "weight").clear
      driver.find_element(:id, "weight").send_keys "1"
      # driver.find_element(:id, "details").send_keys "#{_details}" if _details
      # driver.find_element(:id, "dimensions").send_keys "#{_dimensions}" if _dimensions
      # driver.find_element(:id, "materials").send_keys "#{_materials}"
      # driver.find_element(:id, "availability").clear
      # driver.find_element(:id, "availability").send_keys ""
      # driver.find_element(:id, "care_instructions").clear
      # driver.find_element(:id, "care_instructions").send_keys ''
      # driver.find_element(:id, "shipping_return").clear
      # driver.find_element(:id, "shipping_return").send_keys ''

      # regex = Regexp.new(".#{details[:sku].downcase.gsub(' ', '.')}.", true)
      index = 0
      # byebug
      # image_files.select { |x| regex.match x }.each do |file|
      Dir.glob(Rails.root.join(_3plus_working_dir, "images", "#{details[:image_filename]}")).sort_by{ |f| File.mtime(f) }.each do |file|
        p "image: #{file}"
        if index == 0
          driver.find_element(:name, "images[]").clear
          driver.find_element(:name, "images[]").send_keys(file)
        else
          driver.find_element(:link, "Add More image").click
          driver.find_element(:css, "#childDiv#{index+3} > input[name=\"images[]\"]").clear
          driver.find_element(:css, "#childDiv#{index+3} > input[name=\"images[]\"]").send_keys(file)
        end
        index += 1
      end

      driver.find_element(:name, "defaultimage").click
      driver.find_element(:id, "meta_title").clear
      driver.find_element(:id, "meta_title").send_keys "#{_name}"
      driver.find_element(:id, "meta_keyword").clear
      driver.find_element(:id, "meta_keyword").send_keys "lampu hias, lampu dekorasi, lampu gantung"
      driver.find_element(:id, "meta_description").clear
      driver.find_element(:id, "meta_description").send_keys "Jual lampu hias, lampu gantung, lampu meja, lampu bediri dari LampuOnline. Gratis ongkos kirim se-Jabodetabek."
      
      save_btn = driver.find_element(:id, "save_butn")
      driver.action.move_to(save_btn).perform
      driver.find_element(:id, "save_butn").click

      # # remember uploaded
      uploaded_products << product_id

      # refresh on every completed upload
      CSV.open(Rails.root.join('data', '3plus', 'uploaded_products.csv'), 'wb') do |csv|
        uploaded_products.map { |id| csv << [id] }
      end

      # break
    end

  end
end
