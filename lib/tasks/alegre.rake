require 'open-uri'

namespace :alegre do

  task :import do

    alegre_working_dir = '/Users/rasyidmujahid/Repo/sellers/alegre/rainbow'
    alegre_prices_csv = 'PRICE RAINBOW.csv'
    alegre_product_info_csv = 'Price Product Information.csv'

    product_details = {}

    CSV.foreach(Rails.root.join(alegre_working_dir, alegre_prices_csv), { headers: :true }) do |row|
      details = {
        sku: row[1],
        name: row[2],
        price: row[5],
      }
      product_id = row[1]
      product_details[product_id] = details
    end

    CSV.foreach(Rails.root.join(alegre_working_dir, alegre_product_info_csv), { headers: :true }) do |row|
      product_id = row[1]
      next unless product_details[product_id]
      product_details[product_id][:length] = row[6]
      product_details[product_id][:width] = row[7]
      product_details[product_id][:height] = row[8]
      product_details[product_id][:weight] = row[10]
      product_details[product_id][:materials] = row[11]
    end

    uploaded_products = CSV.foreach(Rails.root.join('data', 'alegre', 'uploaded_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome,
                                     :prefs => prefs,
                                     :desired_capabilities => caps
                                     )
    driver.manage.window.maximize
    driver.manage.timeouts.implicit_wait = 300

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "support@equatorjingga.com"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "equatorjingga"
    driver.find_element(:id, "send2").click
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    image_files = Dir.glob(Rails.root.join(alegre_working_dir + '/*.jpg'))

    limit = 1

    product_details.each do |product_id, details|

      next if uploaded_products.include?(product_id)

      # product official name
      _name = "Rainbow #{details[:name]}"
      p "_name: #{_name}"

      # description
      _description = "<div>#{_name}</div>"

      # sku
      _sku = "alegre-furniture-#{details[:sku].gsub(' ', '-')}"
      p "_sku #{_sku}"

      # price
      _price = details[:price].gsub(/\D/, '').to_i

      # stock
      _stock = 0
  
      _details = details[:spesification].split('.').map(&:strip).reject(&:empty?).join('</li><li>') if details[:spesification]
      _details += details[:advantage].split(',').map(&:strip).map(&:capitalize).reject(&:empty?).join('</li><li>') if details[:advantage]
      _details = "<ul><li>#{_details}</li></ul>" if _details
    
      _dimensions = "<div><li>Length: #{details[:length]} cm</li>"\
        + "<li>Width: #{details[:width]} cm</li>"\
        + "<li>Height: #{details[:height]} cm</li>"\
        + "</div>"
      
      _materials = "<div><li>#{details[:materials]}</li></div>"
      
      if details[:installation]
        _installation = details[:installation].split('.').map(&:strip).reject { |c| c.empty? || c.size < 2 }.join('</li><li>')
        _installation = "<p>Petunjuk pemasangan:</p><ul><li>#{_installation}</li></ul>" 
      end

      _weight = details[:weight].to_f

      driver.find_element(:id, "save_butn").click

      wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      wait.until do
        driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div/div[4]/div[2]/div/div[3]/div[1]").size > 0
      end

      # sleep(3)
      # open category
      driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[3]/span[1]").click

      # wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      # wait.until do
      #   driver.find_elements(:xpath, "//input[@name='category[]'][@value='27']").size > 0
      # end

      sleep(2)

      driver.find_element(:xpath, "//input[@name='category[]'][@value='41']").click
      driver.find_element(:xpath, "//input[@name='category[]'][@value='13']").click
      driver.find_element(:xpath, "//input[@name='category[]'][@value='11']").click
      driver.find_element(:id, "name").clear
      driver.find_element(:id, "name").send_keys "#{_name}"
      driver.find_element(:id, "description").clear
      driver.find_element(:id, "description").send_keys "#{_description}"
      driver.find_element(:id, "descriptions").clear
      driver.find_element(:id, "descriptions").send_keys "#{_description}"
      driver.find_element(:id, "sku").clear
      driver.find_element(:id, "sku").send_keys "#{_sku}"
      driver.find_element(:id, "price").clear
      driver.find_element(:id, "price").send_keys "#{_price}"
      driver.find_element(:id, "stock").clear
      driver.find_element(:id, "stock").send_keys "#{_stock}"
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
      driver.find_element(:id, "weight").clear
      driver.find_element(:id, "weight").send_keys "#{_weight}"
      driver.find_element(:id, "details").send_keys "#{_details}" if _details
      driver.find_element(:id, "dimensions").send_keys "#{_dimensions}"
      driver.find_element(:id, "materials").send_keys "#{_materials}"
      driver.find_element(:id, "availability").clear
      driver.find_element(:id, "availability").send_keys "<ul><li>Proses produksi memerlukan waktu selama 21 hari kerja, terhitung setelah pesanan kami terima.</li></ul>"
      driver.find_element(:id, "care_instructions").clear
      care_instructions = "<ul>"\
        + "<li>Hindari penempatan furniture di ruangan yang memiliki suhu panas, lembab & terkena sinar matahari langsung.</li>"\
        + "<li>Gunakan kain yang lembut untuk membersihkan furniture.</li>"\
        + "<li>Untuk noda yang susah hilang, gunakan sedikit air & sedikit detergent (proses ini jangan dilakukan terlalu lama).</li>"\
        + "</ul>"
      driver.find_element(:id, "care_instructions").send_keys care_instructions
      driver.find_element(:id, "shipping_return").clear
      shipping_return = "<ul>"\
        + "<li>Proses pengiriman dilakukan melalui jasa pengiriman (Tiki, POS, Lorena, Herona dll) ke alamat customer, paling lambat 3 hari setelah proses produksi.</li>"\
        + "<li>Bea pengiriman akan menjadi beban customer.</li>"\
        + "<li>Kami akan berupaya menggunakan jasa pengiriman yang paling murah tetapi tetap menjaga kualitas.</li>"\
        + "<li>Pengiriman dilakukan langsung dari pabrik kami di Cirebon.</li>"\
        + "</ul>"
      driver.find_element(:id, "shipping_return").send_keys shipping_return

      regex = Regexp.new(".#{details[:sku].downcase.gsub(' ', '.')}.", true)
      index = 0
      # byebug
      image_files.select { |x| regex.match x }.each do |file|
      # Dir.glob(Rails.root.join(alegre_working_dir, "/*#{details[:sku].downcase.gsub(' ', '*')}*")).sort_by{ |f| File.mtime(f) }.each do |file|
        p "image: #{file}"
        if index == 0
          driver.find_element(:name, "images[]").clear
          driver.find_element(:name, "images[]").send_keys(file)
        else
          driver.find_element(:link, "Add More image").click
          driver.find_element(:css, "#childDiv#{index+3} > input[name=\"images[]\"]").clear
          driver.find_element(:css, "#childDiv#{index+3} > input[name=\"images[]\"]").send_keys(file)
        end
        index += 1
      end

      driver.find_element(:name, "defaultimage").click
      driver.find_element(:id, "meta_title").clear
      driver.find_element(:id, "meta_title").send_keys "#{_name}"
      driver.find_element(:id, "meta_keyword").clear
      driver.find_element(:id, "meta_keyword").send_keys "#{_name}"
      driver.find_element(:id, "meta_description").clear
      driver.find_element(:id, "meta_description").send_keys "#{_name}"
      
      driver.find_element(:id, "save_butn").click

      # # remember uploaded
      uploaded_products << product_id

      # refresh on every completed upload
      CSV.open(Rails.root.join('data', 'alegre', 'uploaded_products.csv'), 'wb') do |csv|
        uploaded_products.map { |id| csv << [id] }
      end

      # break
    end

  end
end
