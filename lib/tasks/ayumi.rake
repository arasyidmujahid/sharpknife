require 'open-uri'

namespace :ayumi do

  task :grab do

    ayumi_working_dir = '/Users/rasyidmujahid/Repo/sellers/ayumi'
    ayumi_all_items_csv = 'Product List Ayumi Katsuko 21 April 2016.csv'

    product_details = {}

    CSV.foreach(Rails.root.join(ayumi_working_dir, ayumi_all_items_csv), { headers: :true }) do |row|
      next if row[2] == '0'
      details = {
        name: row[2],
        description: row[3],
        material: row[4],
        length: row[5],
        width: row[6],
        height: row[7],
        diameter: row[8],
        price: row[10],
        color: row[11],
        type: row[15]
      }
      product_id = row[2]
      product_details[product_id] = details
    end

    uploaded_products = CSV.foreach(Rails.root.join('data', 'ayumi_katsuko', 'uploaded_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome,
                                     :prefs => prefs,
                                     :desired_capabilities => caps
                                     )
    driver.manage.window.maximize
    driver.manage.timeouts.implicit_wait = 300

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "ayumikatsuko@outlook.com"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "ayumi#123"
    driver.find_element(:id, "send2").click
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    limit = 1

    product_details.each do |product_id, details|

      next if uploaded_products.include?(product_id)

      # product official name
      _name = "Ayumi Katsuko #{details[:name]}"
      p "_name: #{_name}"

      # description
      _desciption = "<div>Ayuki Katsuko #{details[:type]} Synthetic Rattan</div>"

      # sku
      _sku = "ayumi-katsuko-#{product_id.downcase.gsub('\'', '').gsub(' - ', ' ').gsub(' ', '-')}"
      p "_sku #{_sku}"

      # price
      _price = details[:price].gsub(/\D/, '').to_i

      _details = "<div></div>"\
        "<ul>"\
        "<li>Description: #{details[:description]}</li>"\
        "<li>Type: #{details[:type]}</li>"\
        "<li>Color: #{details[:color]}</li>"\
        "</ul>"

      _length = "<li>Length: #{details[:length]} cm</li>" if details[:length]
      _width = "<li>Width #{details[:width]} cm</li>" if details[:width]
      _height = "<li>Height #{details[:height]} cm</li>" if details[:height]
      _diameter = "<li>Diameter #{details[:diameter]} cm</li>" if details[:diameter]
      _dimensions = "<ul>#{_length}#{_width}#{_height}#{_diameter}</ul>"

      _materials = "<div>#{details[:material]}</div>"

      driver.find_element(:id, "save_butn").click

      wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      wait.until do
        driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div/div[4]/div[2]/div/div[3]/div[1]").size > 0
      end

      # sleep(3)

      driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[3]/span[1]").click

      # wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      # wait.until do
      #   driver.find_elements(:xpath, "//input[@name='category[]'][@value='27']").size > 0
      # end

      sleep(2)

      driver.find_element(:xpath, "//input[@name='category[]'][@value='27']").click
      driver.find_element(:id, "name").clear
      driver.find_element(:id, "name").send_keys "#{_name}"
      driver.find_element(:id, "description").clear
      driver.find_element(:id, "description").send_keys "#{_desciption}"
      driver.find_element(:id, "descriptions").clear
      driver.find_element(:id, "descriptions").send_keys "#{_desciption}"
      driver.find_element(:id, "sku").clear
      driver.find_element(:id, "sku").send_keys "#{_sku}"
      driver.find_element(:id, "price").clear
      driver.find_element(:id, "price").send_keys "#{_price}"
      driver.find_element(:id, "stock").clear
      driver.find_element(:id, "stock").send_keys "10"
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
      driver.find_element(:id, "weight").clear
      driver.find_element(:id, "weight").send_keys "1"
      driver.find_element(:id, "details").send_keys "#{_details}"
      driver.find_element(:id, "dimensions").send_keys "#{_dimensions}"
      driver.find_element(:id, "materials").send_keys "#{_materials}"
      driver.find_element(:id, "availability").clear
      driver.find_element(:id, "availability").send_keys "<ul><li>Produk ini adalah PRE ORDER</li><li>Delivery time 4 minggu</li></ul>"
      driver.find_element(:id, "shipping_return").clear
      driver.find_element(:id, "shipping_return").send_keys "<ul><li>Dikirim via JNE Reguler</li><li>Tarif JNE Reguler</li></ul>"

      # byebug

      index = 0
      Dir.glob(Rails.root.join(ayumi_working_dir, "#{details[:name]}*")).sort_by{ |f| File.mtime(f) }.each do |file|
        if index == 0
          driver.find_element(:name, "images[]").clear
          driver.find_element(:name, "images[]").send_keys(file)
        else
          driver.find_element(:link, "Add More image").click
          driver.find_element(:css, "#childDiv#{index+2} > input[name=\"images[]\"]").clear
          driver.find_element(:css, "#childDiv#{index+2} > input[name=\"images[]\"]").send_keys(file)
        end
        index += 1
      end

      driver.find_element(:name, "defaultimage").click
      driver.find_element(:id, "meta_title").clear
      driver.find_element(:id, "meta_title").send_keys "#{_name}"
      driver.find_element(:id, "meta_keyword").clear
      driver.find_element(:id, "meta_keyword").send_keys "#{_name}"
      driver.find_element(:id, "meta_description").clear
      driver.find_element(:id, "meta_description").send_keys "#{_name}"
      driver.find_element(:id, "save_butn").click

      # remember uploaded
      uploaded_products << product_id

      # refresh on every completed upload
      CSV.open(Rails.root.join('data', 'ayumi_katsuko', 'uploaded_products.csv'), 'wb') do |csv|
        uploaded_products.map { |id| csv << [id] }
      end

      # break #if (limit += 1) > 2
    end

  end
end
