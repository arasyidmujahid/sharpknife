require 'open-uri'

namespace :beranda do

  task :import do

    beranda_working_dir = '/Users/rasyidmujahid/Repo/sellers/beranda/'
    beranda_product_info_csv = 'Beranda Living Price.csv'

    product_details = {}

    CSV.foreach(Rails.root.join(beranda_working_dir, beranda_product_info_csv), { headers: :true }) do |row|
      details = {
        name: row[1],
        description: row[2],
        materials: row[3],
        finishing: row[4],
        length: row[5],
        width: row[6],
        seat_depth: row[7],
        seat_depth_sectional: row[8],
        height: row[9],
        seat_height: row[10],
        arm_rest_height: row[11],
        table_top_thickness: row[12],
        seat_thickness: row[13],
        price: row[16],
        color: row[18],
      }
      product_id = row[1]
      product_details[product_id] = details
    end

    uploaded_products = CSV.foreach(Rails.root.join('data', 'beranda', 'uploaded_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome,
                                     :prefs => prefs,
                                     :desired_capabilities => caps
                                     )
    driver.manage.window.maximize
    driver.manage.timeouts.implicit_wait = 300

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "berandaliving@gmail.com"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "beranda123"
    driver.find_element(:id, "send2").click
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    image_files = Dir.glob(Rails.root.join(beranda_working_dir + '/images/*'))

    limit = 1

    product_details.each do |product_id, details|

      next if uploaded_products.include?(product_id)

      # product official name
      _name = "#{details[:name]}"
      p "_name: #{_name}"

      # description
      _description = "<div>#{details[:description]}</div>"
      p "_description: #{_description}"

      # sku
      _sku = "beranda-living-#{details[:name].gsub(' ', '-').downcase}"
      p "_sku #{_sku}"

      # price
      _price = details[:price].gsub(/\D/, '').to_i
      p "_price #{_price}"

      # stock
      _stock = 0
  
      _finishing = details[:finishing]
      _color = details[:color]
      _details = "<ul><li>Finishing: #{_finishing}</li><li>Color: #{_color}</li></ul>"

      _materials = details[:materials].split("\r").join("</li><li>")
      _materials = "<ul><li>#{_materials}</li></ul>"

      _dimensions = "<li>Length: #{details[:length]} cm</li>"\
        + "<li>Width: #{details[:width]} cm</li>"\
        + "<li>Height: #{details[:height]} cm</li>"
      _dimensions += "<li>Seath Depth: #{details[:seat_depth]}</li>" if details[:seat_depth]
      _dimensions += "<li>Seath Depth Sectional: #{details[:seat_depth_sectional]}</li>" if details[:seat_depth_sectional]
      _dimensions += "<li>Seath Height: #{details[:seat_height]}</li>" if details[:seat_height]
      _dimensions += "<li>Arm Rest Heigth: #{details[:arm_rest_height]}</li>" if details[:arm_rest_height]
      _dimensions += "<li>Table Top Thickness: #{details[:table_top_thickness]}</li>" if details[:table_top_thickness]
      _dimensions += "<li>Seat Thickness: #{details[:seat_thickness]}</li>" if details[:seat_thickness]
      _dimensions = "<ul>#{_dimensions}</ul>"

      driver.find_element(:id, "save_butn").click

      wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      wait.until do
        driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div/div[4]/div[2]/div/div[3]/div[1]").size > 0
      end

      # sleep(3)
      # open category
      driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[3]/span[1]").click

      # wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      # wait.until do
      #   driver.find_elements(:xpath, "//input[@name='category[]'][@value='27']").size > 0
      # end

      sleep(2)

      driver.find_element(:xpath, "//input[@name='category[]'][@value='13']").click
      driver.find_element(:xpath, "//input[@name='category[]'][@value='11']").click
      driver.find_element(:id, "name").clear
      driver.find_element(:id, "name").send_keys "#{_name}"
      driver.find_element(:id, "description").clear
      driver.find_element(:id, "description").send_keys "#{_description}"
      driver.find_element(:id, "descriptions").clear
      driver.find_element(:id, "descriptions").send_keys "#{_description}"
      driver.find_element(:id, "sku").clear
      driver.find_element(:id, "sku").send_keys "#{_sku}"
      driver.find_element(:id, "price").clear
      driver.find_element(:id, "price").send_keys "#{_price}"
      driver.find_element(:id, "stock").clear
      driver.find_element(:id, "stock").send_keys "#{_stock}"
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
      driver.find_element(:id, "weight").clear
      driver.find_element(:id, "weight").send_keys "0"
      driver.find_element(:id, "details").send_keys "#{_details}" if _details
      driver.find_element(:id, "dimensions").send_keys "#{_dimensions}"
      driver.find_element(:id, "materials").send_keys "#{_materials}"
      driver.find_element(:id, "availability").clear
      driver.find_element(:id, "availability").send_keys "<ul><li>Proses produksi memerlukan waktu selama 5 minggu, terhitung setelah pesanan kami terima.</li></ul>"
      # driver.find_element(:id, "care_instructions").clear
      # care_instructions = "<ul>"\
      #   + "<li>Hindari penempatan furniture di ruangan yang memiliki suhu panas, lembab & terkena sinar matahari langsung.</li>"\
      #   + "<li>Gunakan kain yang lembut untuk membersihkan furniture.</li>"\
      #   + "<li>Untuk noda yang susah hilang, gunakan sedikit air & sedikit detergent (proses ini jangan dilakukan terlalu lama).</li>"\
      #   + "</ul>"
      # driver.find_element(:id, "care_instructions").send_keys care_instructions
      # driver.find_element(:id, "shipping_return").clear
      # shipping_return = "<ul>"\
      #   + "<li>Proses pengiriman dilakukan melalui jasa pengiriman (Tiki, POS, Lorena, Herona dll) ke alamat customer, paling lambat 3 hari setelah proses produksi.</li>"\
      #   + "<li>Bea pengiriman akan menjadi beban customer.</li>"\
      #   + "<li>Kami akan berupaya menggunakan jasa pengiriman yang paling murah tetapi tetap menjaga kualitas.</li>"\
      #   + "<li>Pengiriman dilakukan langsung dari pabrik kami di Cirebon.</li>"\
      #   + "</ul>"
      # driver.find_element(:id, "shipping_return").send_keys shipping_return

      # regex = Regexp.new(".#{details[:sku].downcase.gsub(' ', '.')}.", true)
      index = 0
      # byebug
      # image_files.select { |x| regex.match x }.each do |file|
      Dir.glob(Rails.root.join(beranda_working_dir, "images", "*#{details[:name]}*")).sort_by{ |f| File.mtime(f) }.each do |file|
        p "image: #{file}"
        if index == 0
          driver.find_element(:name, "images[]").clear
          driver.find_element(:name, "images[]").send_keys(file)
        else
          driver.find_element(:link, "Add More image").click
          driver.find_element(:css, "#childDiv#{index+3} > input[name=\"images[]\"]").clear
          driver.find_element(:css, "#childDiv#{index+3} > input[name=\"images[]\"]").send_keys(file)
        end
        index += 1
      end

      driver.find_element(:name, "defaultimage").click
      driver.find_element(:id, "meta_title").clear
      driver.find_element(:id, "meta_title").send_keys "#{_name}"
      driver.find_element(:id, "meta_keyword").clear
      driver.find_element(:id, "meta_keyword").send_keys "sofa, kursi, meja, prissilia, chair, table, dining chair, mahagony chair"
      driver.find_element(:id, "meta_description").clear
      driver.find_element(:id, "meta_description").send_keys "#{details[:description]}"
      
      driver.find_element(:id, "save_butn").click

      # # remember uploaded
      uploaded_products << product_id

      # refresh on every completed upload
      CSV.open(Rails.root.join('data', 'beranda', 'uploaded_products.csv'), 'wb') do |csv|
        uploaded_products.map { |id| csv << [id] }
      end

      # break
    end

  end
end
