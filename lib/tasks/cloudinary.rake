namespace :cloudinary do

  desc "Get images meta data"
  task :get => :environment do

    max_results = 100
    limit = 0

    response = Cloudinary::Api.resources(max_results: max_results, start_at: Time.new(2015, 12, 23))

    p response

    while response.has_key?('next_cursor')
      response['resources'].each do |resource|

        p resource['url']

        Image.where(cloudinary_public_id: resource['public_id']).each do |image|
          image.width = resource['width'].to_i
          image.height = resource['height'].to_i

          ratio = image.width.to_f / image.height.to_f
          image.dimension_ratio = if ratio < 0.75
            5
          elsif ratio < 1.5
            10
          else
            20
          end

          p "dimension #{image.width}x#{image.height} ratio #{ratio}, cap ratio #{image.dimension_ratio}"
          
          image.save
          # limit += 1
        end
      end

      # break if limit > 10

      response = Cloudinary::Api.resources(
        max_results: max_results, 
        next_cursor: response['next_cursor'])
    end

  end
end