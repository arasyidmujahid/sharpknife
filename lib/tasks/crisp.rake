require 'crisp'

namespace :crisp do

  task :get do

    crisp_client = Crisp::Client.new
    email = 'shop@grya.co.id'
    pas = 'gryaiscool'

    # auth
    crisp_client.user_session.login_with_email(email, pas)

    # get website_id
    website_id = crisp_client.user_websites.get.first['id']
    p "website_id #{website_id}"

    # get or read conversations json
    dir = Rails.root.join('data', 'crisp')
    page = 0

    while true

      conversations = {}
      conversations_file = Rails.root.join(dir, "conversations_#{page}.json")

      if File.exists?(conversations_file)
        p "Read conversations from #{conversations_file}"
        content = File.read(conversations_file)
        conversations = JSON.parse(content) unless content.empty?
      end

      if conversations.empty?
        p "Get conversations page #{page}"
        conversations = crisp_client.website_conversations.list(website_id, page)

        unless conversations.empty?

          File.open(conversations_file, 'w') do |f|
            p "Writing conversations to #{conversations_file}"
            f.write(conversations.to_json)
          end

        end
      end

      break if conversations.empty?

      # get or read conversation message
      conversations.each do |cv|
        session_id = cv['session_id']
        message_file = Rails.root.join(dir, "message_#{session_id}.json")

        conversation = {}

        if File.exists?(message_file)
          p "Read messages from #{message_file}"
          content = File.read(message_file)
          conversation = JSON.parse(content) unless content.empty?
        end

        if conversation.empty?
          p "Get messages for #{session_id}"
          conversation = crisp_client.website_conversation.get_conversation(website_id, session_id)

          File.open(message_file, 'w') do |f|
            p "Writing messages to #{message_file}"
            f.write(conversation.to_json)
          end unless conversation.empty?

        end
      end

      page = page + 1
    end

  end

  task :text do

    dir = Rails.root.join('data', 'crisp')
    page = 0

    output = File.open(Rails.root.join(dir, 'messages.txt'), 'w')

    while true

      conversations_file = Rails.root.join(dir, "conversations_#{page}.json")

      break unless File.exists?(conversations_file)

      content = File.read(conversations_file)
      conversations = JSON.parse(content) unless content.empty?

      conversations.each do |cv|
        session_id = cv['session_id']
        message_file = Rails.root.join(dir, "message_#{session_id}.json")

        conversation = {}

        if File.exists?(message_file)
          p "Read messages from #{message_file}"
          content = File.read(message_file)
          conversation = JSON.parse(content) unless content.empty?

          output << "nickname: #{conversation['meta']['nickname']}\n"
          output << "email   : #{conversation['meta']['email']}\n"

          conversation['messages'].each do |m|
            output << "#{m['from']} : #{m['content']} (#{Time.at(m['timestamp'] / 1000)})\n"
          end
        end

        output << "===================================\n"
        output << "===================================\n"
      end

      page = page + 1

    end

    output.close

  end
end
