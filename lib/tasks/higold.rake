require 'open-uri'

namespace :higold do

  task :import do

    higold_working_dir = '/Users/rasyidmujahid/Repo/sellers/higold'
    higold_all_items_csv = 'Listing Produk ECommerce Grya.csv'

    product_details = {}

    CSV.foreach(Rails.root.join(higold_working_dir, higold_all_items_csv), { headers: :true }) do |row|
      details = {
        name: row[2],
        description: row[3],
        highlights: row[4],
        parts: row[5],
        dimension: row[6],
        weight: row[12],
        code: row[13],
        stock: row[14],
        price: row[15],
      }
      product_id = row[2]
      product_details[product_id] = details
    end

    uploaded_products = CSV.foreach(Rails.root.join('data', 'higold', 'uploaded_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome,
                                     :prefs => prefs,
                                     :desired_capabilities => caps
                                     )
    driver.manage.window.maximize
    driver.manage.timeouts.implicit_wait = 300

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "higold@outlook.com"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "higold123"
    driver.find_element(:id, "send2").click
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    limit = 1

    product_details.each do |product_id, details|

      next if uploaded_products.include?(product_id)

      # product official name
      _name = "HiGOLD #{details[:name]}"
      p "_name: #{_name}"

      # description
      _highlights = details[:highlights].split("\r").join('</li><li>')
      _description = "<span>#{details[:description]}</span><ul><li>#{_highlights}</li></ul>"

      p "description #{_description}"

      # sku
      _sku = "higold-#{product_id.downcase.gsub(/\W+/, '-')}-#{details[:code]}"
      p "_sku #{_sku}"

      # price
      _price = details[:price].gsub(/\D/, '').to_i
      p "price #{_price}"

      _details = "<ul><li>#{details[:parts].split("\r").join('</li><li>')}</li></ul>" if details[:parts]
      p "details #{_details}"

      _dimensions = "<ul><li>#{details[:dimension]}</li></ul>"
      _materials = ''
      _stock = details[:stock].to_i
      _weight = (details[:weight] || '0').gsub(/\D/, '').to_i

      driver.find_element(:id, "save_butn").click

      wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      wait.until do
        driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div/div[4]/div[2]/div/div[3]/div[1]").size > 0
      end

      # sleep(3)

      driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[6]/span[1]").click

      # wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      # wait.until do
      #   driver.find_elements(:xpath, "//input[@name='category[]'][@value='27']").size > 0
      # end

      sleep(2)

      driver.find_element(:xpath, "//input[@name='category[]'][@value='35']").click
      driver.find_element(:id, "name").clear
      driver.find_element(:id, "name").send_keys "#{_name}"
      driver.find_element(:id, "description").clear
      driver.find_element(:id, "description").send_keys "#{_description}"
      driver.find_element(:id, "descriptions").clear
      driver.find_element(:id, "descriptions").send_keys "#{_description}"
      driver.find_element(:id, "sku").clear
      driver.find_element(:id, "sku").send_keys "#{_sku}"
      driver.find_element(:id, "price").clear
      driver.find_element(:id, "price").send_keys "#{_price}"
      driver.find_element(:id, "stock").clear
      driver.find_element(:id, "stock").send_keys "#{_stock}"
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
      driver.find_element(:id, "weight").clear
      driver.find_element(:id, "weight").send_keys "#{_weight}"
      driver.find_element(:id, "details").send_keys "#{_details}"
      driver.find_element(:id, "dimensions").send_keys "#{_dimensions}"
      driver.find_element(:id, "materials").send_keys "#{_materials}"
      driver.find_element(:id, "availability").clear
      driver.find_element(:id, "availability").send_keys "<ul><li>Produk ini <b>ready stock</b>.</li></ul>"
      driver.find_element(:id, "shipping_return").clear
      driver.find_element(:id, "shipping_return").send_keys "<ul><li>Ongkos kirim ke Jabodetabek</li><li>Waktu pengiriman 3-5 hari</li></ul>"

      index = 0
      image_css = '#otherimages > div > input[name="images[]"]'
      Dir.glob(Rails.root.join(higold_working_dir, "*#{details[:code]}*")).sort_by{ |f| File.mtime(f) }.each do |file|
        p "image #{file}"
        if index == 0
          driver.find_element(:name, "images[]").clear
          driver.find_element(:name, "images[]").send_keys(file)
        else
          driver.find_element(:link, "Add More image").click
          driver.find_elements(:css, image_css)[index - 1].clear
          driver.find_elements(:css, image_css)[index - 1].send_keys(file)
          # byebug
          # driver.find_element(:css, '#otherimages > div > input[name="images[]"]').clear
          # driver.find_element(:css, '#otherimages > div > input[name="images[]"]').send_keys(file)
        end
        index += 1
      end

      driver.find_element(:name, "defaultimage").click
      driver.find_element(:id, "meta_title").clear
      driver.find_element(:id, "meta_title").send_keys "#{_name}"
      driver.find_element(:id, "meta_keyword").clear
      driver.find_element(:id, "meta_keyword").send_keys "#{_name}"
      driver.find_element(:id, "meta_description").clear
      driver.find_element(:id, "meta_description").send_keys "#{_name}"
      
      driver.find_element(:id, "save_butn").click

      # remember uploaded
      uploaded_products << product_id

      # refresh on every completed upload
      CSV.open(Rails.root.join('data', 'higold', 'uploaded_products.csv'), 'wb') do |csv|
        uploaded_products.map { |id| csv << [id] }
      end

      # break #if (limit += 1) > 2
    end

  end
end
