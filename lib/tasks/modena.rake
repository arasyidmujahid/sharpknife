require 'open-uri'

namespace :modena do

  desc "grab images"
  task :images do

    limit = 1

    modena_working_dir = '/Users/rasyidmujahid/Repo/sellers/modena/'
    modena_all_items_csv = 'modena cleaning all item details.csv'

    agent = Mechanize.new

    CSV.foreach(Rails.root.join(modena_working_dir, modena_all_items_csv), { headers: :true }) do |row|
      product_id = URI.unescape row[9]

      product_dir = Rails.root.join(modena_working_dir, 'images', product_id)
      Dir.mkdir product_dir unless Dir.exists?(product_dir)

      row[2].split(';').each do |image_url|

        uri = URI.parse(image_url.strip)
        image_filename = URI.unescape(File.basename(uri.path))

        # download image if not exists
        image_file_path = Rails.root.join(product_dir, image_filename)

        if File.exists?(image_file_path) and File.zero?(image_file_path)
          File.delete(image_file_path)
        end

        if File.exists?("#{image_file_path}.1")
          File.delete("#{image_file_path}.1")
        end

        unless File.exists?(image_file_path)
          p "#{image_file_path} #{image_url}"
          begin
            # agent.get(URI.parse(image_url)).save(image_file_path)
            # sleep(1)
            File.open(image_file_path, "wb") do |f|
              f.write(open(URI.parse(image_url)).read)
            end
          rescue Exception => e
            p e
          end
        end
      end

      # break if (limit += 1) > 5
    end
  end

  task :grab do

    modena_working_dir = '/Users/rasyidmujahid/Repo/sellers/modena'
    modena_all_items_csv = 'modena cleaning all item details.csv'
    modena_all_items_magic_csv = 'modena cleaning all item details magic.csv'
    product_details = {}

    CSV.foreach(Rails.root.join(modena_working_dir, modena_all_items_csv), { headers: :true }) do |row|
      details = {
        after_price: row[10],
        name: row[4],
        before_price: row[6],
      }
      product_id = row[9]
      product_details[product_id] = details
    end

    CSV.foreach(Rails.root.join(modena_working_dir, modena_all_items_magic_csv), { headers: :true }) do |row|
      values = row[0].split(';').map(&:strip)
      key = row[1]
      product_id = row[3]

      # + details: fitur, etc
      # + dimensions: dimension*
      # ~ Informasi Pengiriman

      next if key.downcase == 'informasi pengiriman'

      if product_details.has_key?(product_id)
        product_details[product_id][key.downcase] = values
      else
        p "#{product_id} is not found"
      end

    end

    uploaded_products = CSV.foreach(Rails.root.join('data', 'modena', 'uploaded_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome,
                                     :prefs => prefs,
                                     :desired_capabilities => caps
                                     )
    driver.manage.window.maximize
    driver.manage.timeouts.implicit_wait = 300

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "idmodena@gmail.com"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "modena#123"
    driver.find_element(:id, "send2").click
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    limit = 1

    product_details.each do |product_id, details|

      next if uploaded_products.include?(URI.unescape(product_id))

      # product official name
      _name = details[:name].downcase.gsub(' - ', '-').gsub(' ', '-')
      _name = URI.unescape(product_id).sub('cleaning-', '').sub(_name, '').gsub('-', ' ').split.map(&:capitalize).join(' ')
      _name = "Modena #{_name} #{details[:name]}"
      p "_name: #{_name}"

      # description, taken from fitur
      _desciption = if details.has_key?('fitur')
        fitur_list = '<li>' + details['fitur'].join('</li><li>') + '</li>'
        "<ul>#{fitur_list}</ul>"
      else
        _name
      end

      # sku
      _sku = "modena-#{URI.unescape(product_id)}"

      # price
      _before_price = details[:before_price]
      _after_price = details[:after_price]

      # details, # dimensions
      _details = ''
      _dimensions = ''

      details.each do |key, value|

        next if [:name, :before_price, :after_price].include?(key)
        _values = "<div>#{key.capitalize}</div>"
        _values += "<ul><li>#{value.join('</li><li>')}</li></ul></br>"

        if key.include?('dimensi')
          _dimensions += _values
        else
          _details += _values
        end
      end

      driver.find_element(:id, "save_butn").click
      driver.find_element(:xpath, "(//input[@name='category[]'])[9]").click
      driver.find_element(:id, "name").clear
      driver.find_element(:id, "name").send_keys "#{_name}"
      driver.find_element(:id, "description").clear
      driver.find_element(:id, "description").send_keys "#{_desciption}"
      driver.find_element(:id, "descriptions").clear
      driver.find_element(:id, "descriptions").send_keys "#{_desciption}"
      driver.find_element(:id, "sku").clear
      driver.find_element(:id, "sku").send_keys "#{_sku}"
      driver.find_element(:id, "price").clear
      driver.find_element(:id, "price").send_keys "#{_before_price}"
      driver.find_element(:id, "special_price").clear
      driver.find_element(:id, "special_price").send_keys "#{_after_price}"
      driver.find_element(:id, "special_from_date").clear
      driver.find_element(:id, "special_from_date").send_keys "2016:05:10"
      driver.find_element(:id, "special_to_date").clear
      driver.find_element(:id, "special_to_date").send_keys "2216:05:10"
      driver.find_element(:id, "stock").clear
      driver.find_element(:id, "stock").send_keys "10"
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
      driver.find_element(:id, "weight").clear
      driver.find_element(:id, "weight").send_keys "5"
      driver.find_element(:id, "details").send_keys "#{_details}"
      driver.find_element(:id, "dimensions").send_keys "#{_dimensions}"
      driver.find_element(:id, "availability").clear
      driver.find_element(:id, "availability").send_keys "<div>Produk ini tersedia untuk order.</div>"
      driver.find_element(:id, "shipping_return").clear
      driver.find_element(:id, "shipping_return").send_keys "<ul><li>Bebas ongkos kirim Jabodetabek</li><li>Dikirim via JNE Reguler</li></ul>"

      Dir.glob(Rails.root.join(modena_working_dir, 'images', "#{URI.unescape(product_id)}", '*.png')) do |file|
        driver.find_element(:name, "images[]").clear
        driver.find_element(:name, "images[]").send_keys "#{file}"
      end

      index = 2
      Dir.glob(Rails.root.join(modena_working_dir, 'images', "#{URI.unescape(product_id)}", '*.jpg')) do |file|
        driver.find_element(:link, "Add More image").click
        driver.find_element(:css, "#childDiv#{index} > input[name=\"images[]\"]").clear
        driver.find_element(:css, "#childDiv#{index} > input[name=\"images[]\"]").send_keys "#{file}"
        index += 1
      end

      driver.find_element(:name, "defaultimage").click
      driver.find_element(:id, "meta_title").clear
      driver.find_element(:id, "meta_title").send_keys "#{_name}"
      driver.find_element(:id, "meta_keyword").clear
      driver.find_element(:id, "meta_keyword").send_keys "#{_name}"
      driver.find_element(:id, "meta_description").clear
      driver.find_element(:id, "meta_description").send_keys "#{_name}"
      driver.find_element(:id, "save_butn").click
      
      # remember uploaded
      uploaded_products << URI.unescape(product_id)

      # refresh on every completed upload
      CSV.open(Rails.root.join('data', 'modena', 'uploaded_products.csv'), 'wb') do |csv|
        uploaded_products.map { |id| csv << [id] }
      end

      # break if (limit += 1) > 2
    end

  end
end
