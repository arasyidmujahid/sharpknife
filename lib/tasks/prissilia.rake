require 'open-uri'

namespace :prissilia do

  task :import do

    prissilia_working_dir = '/Users/rasyidmujahid/Repo/sellers/prissilia'
    prissilia_product_info_csv = 'Listing Produk ECommerce Prissilia.csv'

    product_details = {}

    CSV.foreach(Rails.root.join(prissilia_working_dir, prissilia_product_info_csv), { headers: :true }) do |row|
      details = {
        code: row[1],
        name: row[2],
        description: row[3],
        advantage: row[4],
        dimensions: row[6],
        weight: row[7],
        stock: row[13],
        price: row[14],
        category: row[17],
      }
      product_id = row[1]
      product_details[product_id] = details
    end

    uploaded_products = CSV.foreach(Rails.root.join('data', 'prissilia', 'uploaded_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome,
                                     :prefs => prefs,
                                     :desired_capabilities => caps
                                     )
    driver.manage.window.maximize
    driver.manage.timeouts.implicit_wait = 300

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "ecom@prissilia.com"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "prissilia123"
    driver.find_element(:id, "send2").click
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    image_files = Dir.glob(Rails.root.join(prissilia_working_dir, 'images', '*'))

    limit = 1

    product_details.each do |product_id, details|

      next if uploaded_products.include?(product_id)

      # product official name
      _name = "#{details[:name]}"
      p "_name: #{_name}"

      # description
      _description = "<div>#{details[:advantage]}</div>"
      p "_description: #{_description}"

      # sku
      _sku = "prissilia-#{details[:name].gsub(' ', '-').downcase}-#{details[:code]}"
      _sku = "prissilia-#{details[:code]}" if _sku.length > 64
      p "_sku #{_sku}"

      # price
      _price = details[:price].gsub(/\D/, '').to_i

      # stock
      _stock = details[:stock]

      _details = "<div>#{details[:description]}</div>"
      p "_details: #{_details}"
    
      _dimensions = "<div>#{details[:dimensions]}</div>"
      p "_dimensions: #{_dimensions}"
      
      # _materials = "<div><li>#{details[:materials]}</li></div>"
      
      _weight = details[:weight].to_f

      driver.find_element(:id, "save_butn").click

      wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      wait.until do
        driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div/div[4]/div[2]/div/div[3]/div[1]").size > 0
      end

      # sleep(3)
      # open category
      case details[:category]
      when 'Penyimpanan'
        driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[3]/span[1]").click
      when 'Meja Kursi'
        driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[3]/span[1]").click
      when 'Ranjang'
        driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[5]/span[1]").click
      when 'Rak Dapur'
        driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[6]/span[1]").click
      when 'Bantal'
        driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[3]/span[1]").click
      when 'Sofa'
        driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[3]/span[1]").click
      end

      # wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      # wait.until do
      #   driver.find_elements(:xpath, "//input[@name='category[]'][@value='27']").size > 0
      # end

      sleep(2)

      case details[:category]
      when 'Penyimpanan'
        driver.find_element(:xpath, "//input[@name='category[]'][@value='11']").click
        driver.find_element(:xpath, "//input[@name='category[]'][@value='41']").click
      when 'Meja Kursi'
        driver.find_element(:xpath, "//input[@name='category[]'][@value='11']").click
        driver.find_element(:xpath, "//input[@name='category[]'][@value='13']").click
      when 'Ranjang'
        driver.find_element(:xpath, "//input[@name='category[]'][@value='9']").click
        driver.find_element(:xpath, "//input[@name='category[]'][@value='14']").click
      when 'Rak Dapur'
        driver.find_element(:xpath, "//input[@name='category[]'][@value='16']").click
        driver.find_element(:xpath, "//input[@name='category[]'][@value='35']").click
      when 'Bantal'
        driver.find_element(:xpath, "//input[@name='category[]'][@value='11']").click
        driver.find_element(:xpath, "//input[@name='category[]'][@value='12']").click
      when 'Sofa'
        driver.find_element(:xpath, "//input[@name='category[]'][@value='11']").click
        driver.find_element(:xpath, "//input[@name='category[]'][@value='12']").click
      end

      driver.find_element(:id, "name").clear
      driver.find_element(:id, "name").send_keys "#{_name}"
      driver.find_element(:id, "description").clear
      driver.find_element(:id, "description").send_keys "#{_description}"
      driver.find_element(:id, "descriptions").clear
      driver.find_element(:id, "descriptions").send_keys "#{_description}"
      driver.find_element(:id, "sku").clear
      driver.find_element(:id, "sku").send_keys "#{_sku}"
      driver.find_element(:id, "price").clear
      driver.find_element(:id, "price").send_keys "#{_price}"
      driver.find_element(:id, "stock").clear
      driver.find_element(:id, "stock").send_keys "#{_stock}"
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
      driver.find_element(:id, "weight").clear
      driver.find_element(:id, "weight").send_keys "#{_weight}"
      driver.find_element(:id, "details").clear
      driver.find_element(:id, "details").send_keys "#{_details}" if _details
      driver.find_element(:id, "dimensions").clear
      driver.find_element(:id, "dimensions").send_keys "#{_dimensions}"
      # driver.find_element(:id, "materials").clear
      # driver.find_element(:id, "materials").send_keys "#{_materials}"
      driver.find_element(:id, "availability").clear
      driver.find_element(:id, "availability").send_keys "<ul><li>Proses produksi memerlukan waktu selama 14 hari, terhitung setelah pesanan kami terima.</li></ul>"
      # driver.find_element(:id, "care_instructions").clear
      # driver.find_element(:id, "care_instructions").send_keys ""
      driver.find_element(:id, "shipping_return").clear
      driver.find_element(:id, "shipping_return").send_keys "<ul><li>Waktu pengiriman 2-3 hari</li></ul>"

      regex = Regexp.new(".#{details[:code]}.", true)
      index = 0
      # byebug
      image_files.select { |x| regex.match x }.each do |file|
      # Dir.glob(Rails.root.join(prissilia_working_dir, "/*#{details[:sku].downcase.gsub(' ', '*')}*")).sort_by{ |f| File.mtime(f) }.each do |file|
        p "image: #{file}"
        if index == 0
          driver.find_element(:name, "images[]").clear
          driver.find_element(:name, "images[]").send_keys(file)
        else
          driver.find_element(:link, "Add More image").click
          driver.find_element(:css, "#childDiv#{index+1} > input[name=\"images[]\"]").clear
          driver.find_element(:css, "#childDiv#{index+1} > input[name=\"images[]\"]").send_keys(file)
        end
        index += 1
      end

      driver.find_element(:name, "defaultimage").click
      driver.find_element(:id, "meta_title").clear
      driver.find_element(:id, "meta_title").send_keys "#{_name}"
      driver.find_element(:id, "meta_keyword").clear
      driver.find_element(:id, "meta_keyword").send_keys "furniture, meja, kursi, rak, lemari, dekorasi"
      driver.find_element(:id, "meta_description").clear
      driver.find_element(:id, "meta_description").send_keys "#{details[:advantage]}"
      
      # begin
      #   driver.find_element(:id, "save_butn").click  
      # rescue Exception => e
      #   p e
      #   wait = Selenium::WebDriver::Wait.new(:timeout => 30)
      #   wait.until do
      #     driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div/div[4]/div[2]/div/div[3]/div[1]").size > 0
      #   end
      # end
      
      # # remember uploaded
      uploaded_products << product_id

      # refresh on every completed upload
      CSV.open(Rails.root.join('data', 'prissilia', 'uploaded_products.csv'), 'wb') do |csv|
        uploaded_products.map { |id| csv << [id] }
      end

      break
    end

  end
end
