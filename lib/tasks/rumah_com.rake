require "csv"

namespace :rumah_com do

  desc "scrap"
  task :email => :environment do
    base_path = 'http://www.rumah.com/property-search/email'
    category_ids = {
      'new-homes' => 3,
      'property-listings' => 7,
      'commercial' => 12,
      'agent-directory' => 1,
      'property-news' => 9,
      'questions-and-answers' => 8
    }

    # selector to url list
    list_item_selector = 'div.slisting_item h4 a'
    # list_item_selector = 'div.slisting_item div.providerdetails a'

    # selector to whole page where contains email
    page_content_selector = 'div#wrapper'

    # selector to check if page to scrap is ready
    page_detail_selector = 'div#wrapper'

    emails_csv = CSV.open(Rails.root.join('data', 'emails.csv'), 'a+')

    # Input capabilities
    # caps = Selenium::WebDriver::Remote::Capabilities.new
    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'
    
    profile = Selenium::WebDriver::Chrome::Profile.new
    profile["profile.default_content_settings"] = { :images => '2' }
    profile.add_extension(File.join(Dir.pwd, 'extension_5_4_9.crx'))

    prefs = {
      :webkit => {
        :webprefs => {
          :loads_images_automatically => false
        }
      }, 
      :app_list => {
        :profile => "~/Library/Application Support/Google/Chrome/Default"
      },
      :extensions => {
        :disabled => false
      }
    }

    # driver = Selenium::WebDriver.for(:remote,
    #   :url => "http://abdurrasyidmujah1:iCtHAnTAcomcqBaxXZnZ@hub.browserstack.com/wd/hub",
    #   :desired_capabilities => caps)
    
    extension_dir = "/Users/rasyidmujahid/Library/Application Support/Google/Chrome/Default/Extensions/mlomiejdfkolichcflejclcbmpeaniij/5.4.9_0"
    user_data_dir = "/var/folders/37/115xzqh904b89dhgl4nv2qsc0000gp/T/.org.chromium.Chromium."

    data_dir_ids = {
      'JZOAaR' => 56,
      'qIfqfe' => 1
    }

    used_data_dir_id = 'JZOAaR'
    page = data_dir_ids[used_data_dir_id]

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')
    driver = Selenium::WebDriver.for(:chrome, 
      :prefs => prefs, 
      :desired_capabilities => caps,
      :switches => ["user-data-dir=#{user_data_dir}#{used_data_dir_id}"]
    )
    driver.manage.window.maximize

    url_path = "#{base_path}/#{category_ids['questions-and-answers']}/#{page}"
    file_name = "#{url_path.gsub('/','_')}.html"
    file_path = Rails.root.join('data', file_name)

    p "navigating to #{url_path}"

    driver.navigate.to "#{url_path}"
    sleep(10)

    loop do

      page = driver.page_source
      p [driver.current_url, page.include?('captcha') ? 'captcha' : 'OK'].join(',')

      File.open(file_path, "w") do |file|
        file << page
      end

      begin

        wait = Selenium::WebDriver::Wait.new(:timeout => 10)
        wait.until do
          driver.find_elements(:css => "#homeleft").size > 0
        end

      rescue Exception => e

        while page.include?('captcha')
          img = driver.find_element(css: 'img#recaptcha_challenge_image')
          p "BREAK THE CAPTCHA #{img['src']}"

          sleep(10)
          answer = File.read(Rails.root.join('data', 'captcha'))
          input = driver.find_element(:id, 'recaptcha_response_field')
          input.send_keys answer
          input.submit

          begin
            wait = Selenium::WebDriver::Wait.new(:timeout => 30)
            wait.until do
              driver.find_elements(:css => "#homeleft").size > 0
            end
          rescue Exception => e
          end

          page = driver.page_source
          File.open(file_path, "w") do |file|
            file << page
          end

        end
      end

      sleep(2)

      driver.find_elements(css: "#{list_item_selector}").each do |link|

        url = link['href'].gsub('http://www.rumah.com/', '')
        p "#{url}"
        
        file_name = "#{url.gsub('/','_')}.html"
        file_name = "#{url.gsub('/','_')[0..255]}.html" if file_name.length > 255
        
        file_path = Rails.root.join('data', file_name)

        if File.exists?(file_path)
          p "Exists #{file_path}"
          next
          html = File.open(file_path, 'r') do |file|
            file.read
          end
        else
          driver.action.key_down(:command).click(link).key_up(:command).perform
          sleep(2)
        end
      end

      # wait = Selenium::WebDriver::Wait.new(:timeout => 30)
      # wait.until do
      #   driver.window_handles.size > 5
      # end

      while driver.window_handles.size > 1
        
        driver.switch_to.window(driver.window_handles[1])

        begin
          wait = Selenium::WebDriver::Wait.new(:timeout => 10)
          wait.until do
            driver.find_elements(:css => "#{page_detail_selector}").size > 0
          end
          
          p "new_tab #{driver.current_url}"
        
        rescue Exception => e
          page = driver.page_source
          
          while page.include?('captcha')
            img = driver.find_element(css: 'img#recaptcha_challenge_image')
            p "BREAK THE CAPTCHA #{driver.current_url} #{img['src']}"

            sleep(10)
            answer = File.read(Rails.root.join('data', 'captcha'))
            input = driver.find_element(:id, 'recaptcha_response_field')
            input.send_keys answer
            input.submit

            begin
              wait = Selenium::WebDriver::Wait.new(:timeout => 30)
              wait.until do
                driver.find_elements(:css => "#{page_detail_selector}").size > 0
              end  
            rescue Exception => e
            end
            page = driver.page_source
          end
        end

        html = driver.page_source

        File.open(file_path, "w") do |file|
          file << html
        end

        html = driver.find_element(css: "#{page_content_selector}").text

        reg = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i

        if html
          emails = html.scan(reg).uniq
          p "emails: #{emails}"
          emails_csv << emails
        end

        driver.close
      end

      driver.switch_to.window(driver.window_handles[0])

      ## pagination
      pagination = driver.find_elements(css: '.pagination a')
      
      if pagination
        
        next_page = pagination.select { |link| link.text == 'Berikutnya' }.first

        if next_page.present?
          p "next_page #{next_page['href']}"
          driver.action.click(next_page).perform
        else
          break
        end
        
        sleep(rand(2..5))
      else
        break
      end

    end

    driver.quit
    emails_csv.close

  end

end
