require 'open-uri'

namespace :sango do

  task :import do

    sango_working_dir = '/Users/rasyidmujahid/Repo/sellers/sango/'
    sango_product_info_csv = 'sango-piring.csv'

    product_details = {}

    CSV.foreach(Rails.root.join(sango_working_dir, sango_product_info_csv), { headers: :true }) do |row|
      details = {
        name: row[2],
        type: row[3],
        description: row[4],
        stock: row[5],
        price: row[6],
      }
      product_id = row[2]
      product_details[product_id] = details
    end

    uploaded_products = CSV.foreach(Rails.root.join('data', 'sango', 'uploaded_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome, :prefs => prefs, :desired_capabilities => caps)

    # driver.manage.timeouts.implicit_wait = 300
    driver.manage.window.maximize

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "sango@outlook.com"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "sangosango"
    # driver.find_element(:id, "send2").click
    save_btn = driver.find_element(:id, "send2")
    driver.action.move_to(save_btn).click(save_btn).perform
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    sleep(10)

    # image_files = Dir.glob(Rails.root.join(sango_working_dir + '/images/*'))

    limit = 1

    product_details.each do |product_id, details|

      next if uploaded_products.include?(product_id)

      # product official name
      if details[:name].downcase.include?(details[:type].downcase)
        _name = details[:name]
      else
        _name = "#{details[:type]} #{details[:name]}"
      end
      _name = _name.split.map(&:capitalize)*' '
      p "_name: #{_name}"

      # description
      if details[:description]
        _description = details[:description].gsub('\n', '<br>').split(';').join("</li><li>")
        _description = "<ul><li>#{_description}</li></ul>"
      else
        _description = details[:name]
      end
      p "_description: #{_description}"

      # sku
      _sku = "sango-#{_name.gsub(' ', '-').downcase}"
      if _sku.length > 64
        how_many = 64 - _sku.length - 1
        _sku = "sango-#{details[:name][0..how_many].gsub(' ', '-').downcase}"
      end
      p "_sku #{_sku}"

      # price
      _price = details[:price].gsub(/\D/, '').to_i
      p "_price #{_price}"

      # stock
      _stock = details[:stock].to_i

      # details
      # if details[:details]
      #   _details = details[:details].split(';').join("</li><li>")
      #   _details = "<ul><li>#{_details}</li></ul>"
      #   p "_details #{_details}"
      # end

      # _finishing = details[:finishing]
      # _color = details[:color]
      # _details = "<ul><li>Finishing: #{_finishing}</li><li>Color: #{_color}</li></ul>"

      # _materials = details[:materials].split("\r").join("</li><li>")
      # _materials = "<ul><li>#{_materials}</li></ul>"

      # _dimensions = details[:dimensions] if details[:dimensions]

      # _availability = '<ul><li>Proses produksi 2 Minggu ( Order tidak full, Cuaca mendukung )</li><li>Kami kirimkan laporan produksi tiap 3 hari</li><li>Apabila ada perubahan segera konfirmasi</li><li>Laporan kami kirimkan dalam bentuk foto maupun video</li></ul>'

      # _shipping = '<ul><li>Pengiriman bisa ke seluruh wilayah di indonesia dan luar negeri</li><li>Pengiriman dari kota Jepara</li><li>Biaya pengiriman di tanggung pembeli</li><li>Order hanya bisa di batalkan jika belum masuk tahap proses produksi</li><li>Jika di batalkan sebelum proses produksi berlangsung uang kami kembalikan secara utuh</li></ul>'

      _weight = 1

      driver.find_element(:id, "save_butn").click

      wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      wait.until do
        driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div[1]/div[2]/div/div[3]/div[3]").size > 0
      end


      # sleep(3)
      # open category
      driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[10]/span[1]").click
      sleep(2)

      driver.find_element(:xpath, "//input[@name='category[]'][@value='45']").click
      driver.find_element(:xpath, "//input[@name='category[]'][@value='83']").click

      driver.find_element(:id, "name").clear
      driver.find_element(:id, "name").send_keys "#{_name}"
      driver.find_element(:id, "description").clear
      driver.find_element(:id, "description").send_keys "#{_description}"
      driver.find_element(:id, "descriptions").clear
      driver.find_element(:id, "descriptions").send_keys "#{_description}"
      driver.find_element(:id, "sku").clear
      driver.find_element(:id, "sku").send_keys "#{_sku}"
      driver.find_element(:id, "price").clear
      driver.find_element(:id, "price").send_keys "#{_price}"
      driver.find_element(:id, "stock").clear
      driver.find_element(:id, "stock").send_keys "#{_stock}"
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
      driver.find_element(:id, "weight").clear
      driver.find_element(:id, "weight").send_keys "#{_weight}"
      # driver.find_element(:id, "details").send_keys "#{_details}" if _details
      # driver.find_element(:id, "dimensions").send_keys "#{_dimensions}" if _dimensions
      # driver.find_element(:id, "materials").send_keys "#{_materials}"
      # driver.find_element(:id, "availability").clear
      # driver.find_element(:id, "availability").send_keys _availability
      # driver.find_element(:id, "care_instructions").clear
      # driver.find_element(:id, "care_instructions").send_keys ''
      # driver.find_element(:id, "shipping_return").clear
      # driver.find_element(:id, "shipping_return").send_keys _shipping

      index = 0
      # byebug
      Dir.glob(Rails.root.join(sango_working_dir, "images", "*#{details[:name]}*")).sort_by{ |f| File.mtime(f) }.each do |file|
        p "image: #{file}"
        if index == 0
          driver.find_element(:name, "images[]").clear
          driver.find_element(:name, "images[]").send_keys(file)
        else
          driver.find_element(:link, "Add More image").click
          driver.find_element(:css, "#childDiv#{index+9} > input[name=\"images[]\"]").clear
          driver.find_element(:css, "#childDiv#{index+9} > input[name=\"images[]\"]").send_keys(file)
        end
        index += 1
      end

      driver.find_element(:name, "defaultimage").click
      driver.find_element(:id, "meta_title").clear
      driver.find_element(:id, "meta_title").send_keys "#{_name}"
      driver.find_element(:id, "meta_keyword").clear
      driver.find_element(:id, "meta_keyword").send_keys "sango"
      driver.find_element(:id, "meta_description").clear
      driver.find_element(:id, "meta_description").send_keys "Jual perlengkapan ruang makan dan tableware dari bahan keramik."

      save_btn = driver.find_element(:id, "save_butn")
      driver.action.move_to(save_btn).perform
      driver.find_element(:id, "save_butn").click

      # # remember uploaded
      uploaded_products << product_id

      # refresh on every completed upload
      CSV.open(Rails.root.join('data', 'sango', 'uploaded_products.csv'), 'wb') do |csv|
        uploaded_products.map { |id| csv << [id] }
      end

      # break
    end

  end
end
