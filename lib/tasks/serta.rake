require 'open-uri'

namespace :serta do

  task :import do

    serta_working_dir = '/Users/rasyidmujahid/Repo/sellers/AGM/Serta'
    serta_all_items_csv = 'List Product Online Serta 2016.csv'

    product_details = {}

    CSV.foreach(Rails.root.join(serta_working_dir, serta_all_items_csv), { headers: :true }) do |row|
      details = {
        name: row[0],
        category: row[2],
        description: row[3],
        comfort_level: row[4],
        thickness: row[5],
        headboard: row[6],
        divan: row[7],
        storage_type: row[8],
        material: row[10],
        before_price: row[13],
        after_price: row[15]
      }
      details.map { |k, v| v.strip! if v }
      product_id = details[:name]
      product_details[product_id] = details
    end

    configurable_products = CSV.foreach(Rails.root.join('data', 'serta', 'configurable_products.csv')).inject({}) { |mem, var| mem[var[0]] = var[1]; mem }
    simple_products = CSV.foreach(Rails.root.join('data', 'serta', 'simple_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome,
                                     :prefs => prefs,
                                     :desired_capabilities => caps
                                     )
    driver.manage.window.maximize
    driver.manage.window.resize_to(1440, 900)
    driver.manage.timeouts.implicit_wait = 300

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "agmserta@outlook.com"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "agmsert4"
    driver.find_element(:id, "send2").click
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    limit = 1

    product_details.each do |product_id, details|

      next if product_id == 'Ergo Pedic'

      # product official name
      _name = "Serta #{details[:category]} #{details[:name]}"
      p "_name: #{_name}"

      # description
      _description = "<div>#{details[:description]}</div>"

      # sku
      _sku = "serta-#{[details[:category], product_id].map(&:strip).join('-').downcase.gsub(' ', '-')}"
      p "_sku #{_sku}"

      # price
      _before_price = details[:before_price].gsub(/\D/, '').to_i
      _after_price = details[:after_price].gsub(/\D/, '').to_i

      _details = "<div>"\
        "<ul>"\
        "<li>Collection: #{details[:description]}</li>"\
        "<li>Category: #{details[:category]}</li>"\
        "<li>Comfort Level: #{details[:comfort_level]}</li>"\
        "<li>Headboard: #{details[:headboard]}</li>"\
        "<li>Divan: #{details[:divan]}</li>"\
        "<li>Storage Type: #{details[:storage_type]}</li>"\
        "<li>Mattress Thickness: #{details[:thickness]}</li>"\
        "</ul></div>"
      p "details #{_details}"

      if details[:material]
        _materials = details[:material].split(',').map(&:strip).join('</li><li>')
        _materials = "<div><li>#{_materials}</li></div>"
        p "materials #{_materials}"
      end

      _dimensions = "<p>Available Sizes : </p>"\
        "<ul>"\
        "<li>Single (100 x 200)</li>"\
        "<li>Single (120 x 200)</li>"\
        "<li>Queen (160 x 200)</li>"\
        "<li>King (180 x 200)</li>"\
        "<li>Super King (200 x 200)</li>"\
        "</ul>"
      p "dimensions #{_dimensions}"

      _configurable_name = details[:name][0..-5].strip
      p "configurable_name #{_configurable_name}"

      unless configurable_products.keys.include?(_configurable_name)

        driver.get(base_url + "/marketplace/marketplaceaccount/new/") unless driver.current_url.include?('marketplaceaccount/new/')

        # create configurable product
        Selenium::WebDriver::Support::Select.new(driver.find_element(:name, "type")).select_by(:text, "Configurable")
        driver.find_element(:id, "save_butn").click
        driver.find_element(:id, "attribute[bed_size]").click
        driver.find_element(:class, "configattr").click

        wait = Selenium::WebDriver::Wait.new(:timeout => 3)
        wait.until do
          driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div/div[4]/div[2]/div/div[3]/div[1]").size > 0
        end

        # # sleep(3)

        driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[5]/span[1]").click

        wait = Selenium::WebDriver::Wait.new(:timeout => 3)
        wait.until do
          driver.find_elements(:xpath, "//input[@name='category[]'][@value='8']").size > 0
        end

        sleep(2)

        _configurable_title = "Serta #{details[:category]} #{_configurable_name}"

        driver.find_element(:xpath, "//input[@name='category[]'][@value='8']").click
        driver.find_element(:xpath, "//input[@name='category[]'][@value='9']").click
        driver.find_element(:id, "name").clear
        driver.find_element(:id, "name").send_keys "#{_configurable_title}"
        driver.find_element(:id, "description").clear
        driver.find_element(:id, "description").send_keys "#{_description}"
        driver.find_element(:id, "descriptions").clear
        driver.find_element(:id, "descriptions").send_keys "#{_description}"
        driver.find_element(:id, "sku").clear
        driver.find_element(:id, "sku").send_keys "#{_sku[0..-5].strip}"
        driver.find_element(:id, "price").clear
        driver.find_element(:id, "price").send_keys "1"
        Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
        Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
        Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
        driver.find_element(:id, "details").send_keys "#{_details}"
        driver.find_element(:id, "dimensions").send_keys "#{_dimensions}"
        driver.find_element(:id, "materials").send_keys "#{_materials}" if _materials
        driver.find_element(:id, "availability").clear
        driver.find_element(:id, "availability").send_keys "<ul><li>Produk ini dapat dipesan dengan masa produksi 7 hari.</li></ul>"
        driver.find_element(:id, "shipping_return").clear
        driver.find_element(:id, "shipping_return").send_keys "<ul><li>Bebas ongkos kirim untuk Jabodetabek</li></ul>"

        # # byebug

        index = 0
        ['No BG', 'BG'].each do |folder|
          Dir.glob(Rails.root.join(serta_working_dir, folder, "#{_configurable_name.downcase}*")).sort_by{ |f| File.mtime(f) }.each do |file|
            p "image #{file}"
            if index == 0
              driver.find_element(:name, "images[]").clear
              driver.find_element(:name, "images[]").send_keys(file)
            else
              driver.find_element(:link, "Add More image").click
              driver.find_element(:css, "#otherimages > div > input[name=\"images[]\"]").clear
              driver.find_element(:css, "#otherimages > div > input[name=\"images[]\"]").send_keys(file)
            end
            index += 1
          end
        end

        driver.find_elements(:name, "defaultimage").first.click
        driver.find_element(:id, "meta_title").clear
        driver.find_element(:id, "meta_title").send_keys "#{_configurable_title}"
        driver.find_element(:id, "meta_keyword").clear
        driver.find_element(:id, "meta_keyword").send_keys "#{_configurable_title}"
        driver.find_element(:id, "meta_description").clear
        driver.find_element(:id, "meta_description").send_keys "#{_configurable_title}"

        begin
          driver.find_element(:id, "save_butn").click
        rescue Exception => e
          wait = Selenium::WebDriver::Wait.new(:timeout => 600)
          wait.until do
            driver.current_url.include?('editapprovedconfigurable')
          end
        end

        current_url = driver.current_url
        magento_product_id = current_url.split('/').last

        # remember uploaded
        configurable_products[_configurable_name] = magento_product_id

        # refresh on every completed upload
        CSV.open(Rails.root.join('data', 'serta', 'configurable_products.csv'), 'wb') do |csv|
          configurable_products.map { |name, id| csv << [name, id] }
        end

      end

      # upload simple product, go to configurableassociate page
      unless simple_products.include?(product_id)

        configurable_product_url = "/marketplace/marketplaceaccount/editapprovedconfigurable/id/#{configurable_products[_configurable_name]}"
        unless driver.current_url.include?(configurable_product_url)
          p "add simple product, go to #{configurable_product_url}"
          driver.get(base_url + configurable_product_url)
        end

        driver.find_element(:id, "associateproduct").click

        driver.find_element(:name, "name").clear
        driver.find_element(:name, "name").send_keys "#{_name}"
        driver.find_element(:id, "sku").clear
        driver.find_element(:id, "sku").send_keys "#{_sku}"
        driver.find_element(:name, "weight").clear
        driver.find_element(:name, "weight").send_keys "50"
        Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "simple_product_visibility")).select_by(:text, "Not Visible Individually")
        driver.find_element(:name, "qty").clear
        driver.find_element(:name, "qty").send_keys "1"

        bed_size_attr_value = nil
        case _sku[-3..-1]
        when '100'
          bed_size_attr_value = 11
        when '120'
          bed_size_attr_value = 12
        when '160'
          bed_size_attr_value = 13
        when '180'
          bed_size_attr_value = 14
        when '200'
          bed_size_attr_value = 15
        end
        p "bed_size_attr_value #{bed_size_attr_value}"
        Selenium::WebDriver::Support::Select.new(driver.find_element(:name, "bed_size")).select_by(:value, bed_size_attr_value.to_s)

        custattr_id = driver.find_element(:name, "bed_size")['id']
        p "custattr_id #{custattr_id}"
        driver.find_element(:name, "bed_size|price|#{custattr_id}|#{bed_size_attr_value}").clear
        driver.find_element(:name, "bed_size|price|#{custattr_id}|#{bed_size_attr_value}").send_keys "1"

        driver.find_element(:id, "save_butn").click

        # remember uploaded
        simple_products << product_id

        # refresh on every completed upload
        CSV.open(Rails.root.join('data', 'serta', 'simple_products.csv'), 'wb') do |csv|
          simple_products.map { |id| csv << [id] }
        end

      end

      # edit simple product prices
      unless simple_products.include?("#{product_id}.price")

        configurable_product_url = "/marketplace/marketplaceaccount/editapprovedconfigurable/id/#{configurable_products[_configurable_name]}"
        unless driver.current_url.include?(configurable_product_url)
          p "edit price, go to #{configurable_product_url}"
          driver.get(base_url + configurable_product_url)
        end

        driver.find_element(:id, "associateproduct").click

        # go to simple product page to edit prices
        simple_product_id = nil
        (1..5).each do |id|
          product_title = driver.find_element(:xpath, "//*[@id='form-associate']/div/table/tbody/tr[#{id}]/td[3]").text
          p "product_title #{product_title}"
          if product_title == _name
            # simple_product_id = driver.find_element(:xpath, "//*[@id='form-associate']/div/table/tbody/tr[#{id}]/td[2]").text
            driver.find_element(:xpath, "//*[@id='form-associate']/div/table/tbody/tr[#{id}]/td[9]").click
            break
          end
        end

        # update prices
        driver.find_element(:id, "price").clear
        driver.find_element(:id, "price").send_keys "#{_before_price}"
        driver.find_element(:id, "special_price").clear
        driver.find_element(:id, "special_price").send_keys "#{_after_price}"
        driver.find_element(:id, "special_from_date").clear
        driver.find_element(:id, "special_from_date").send_keys "2016:05:10"
        driver.find_element(:id, "special_to_date").clear
        driver.find_element(:id, "special_to_date").send_keys "3016:05:10"

        driver.find_element(:id, "save_butn").click

        # remember updated prices
        simple_products << "#{product_id}.price"

        # refresh on every completed upload
        CSV.open(Rails.root.join('data', 'serta', 'simple_products.csv'), 'wb') do |csv|
          simple_products.map { |id| csv << [id] }
        end

        driver.navigate.back
        driver.navigate.back

        # limit += 1
      end

      # break if limit > 2
    end

  end
end
