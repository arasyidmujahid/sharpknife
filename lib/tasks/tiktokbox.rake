require 'open-uri'

namespace :tiktokbox do

  task :import do

    tiktokbox_working_dir = '/Users/rasyidmujahid/Repo/sellers/tiktokbox/'
    tiktokbox_product_info_csv = 'tiktokbox.csv'

    product_details = {}

    CSV.foreach(Rails.root.join(tiktokbox_working_dir, tiktokbox_product_info_csv), { headers: :true }) do |row|
      details = {
        name: row[1],
        description: row[2],
        price: row[3],
        image_1: row[6],
        image_2: row[8],
        image_3: row[10],
        image_4: row[12],
        image_5: row[14]
      }
      product_id = row[1]
      product_details[product_id] = details
    end

    uploaded_products = CSV.foreach(Rails.root.join('data', 'tiktokbox', 'uploaded_products.csv')).map { |r| r[0] }

    prefs = {}

    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    caps['browser'] = 'Chrome'
    caps['browser_version'] = '47.0'
    caps['os'] = 'OS X'
    caps['os_version'] = 'El Capitan'
    caps['resolution'] = '1920x1080'

    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')

    driver = Selenium::WebDriver.for(:chrome, :prefs => prefs, :desired_capabilities => caps)

    # driver.manage.timeouts.implicit_wait = 300
    driver.manage.window.maximize

    base_url = "http://shop.grya.co.id/"

    driver.get(base_url + "/customer/account/login")
    driver.find_element(:id, "email").clear
    driver.find_element(:id, "email").send_keys "shop@grya.co.id"
    driver.find_element(:id, "pass").clear
    driver.find_element(:id, "pass").send_keys "gryashop2016"
    # driver.find_element(:id, "send2").click
    save_btn = driver.find_element(:id, "send2")
    driver.action.move_to(save_btn).click(save_btn).perform
    driver.get(base_url + "/marketplace/marketplaceaccount/new/")

    # image_files = Dir.glob(Rails.root.join(tiktokbox_working_dir + '/images/*'))

    limit = 1

    product_details.each do |product_id, details|

      next if uploaded_products.include?(product_id)

      # product official name
      _name = details[:name].split.map(&:capitalize)*' '
      p "_name: #{_name}"

      # description
      _description = details[:description].split(';').map(&:strip)
      _description = "<p>#{_description.first}</p><ul><li>#{_description[1..-1].join("</li><li>")}</li></ul>"
      p "_description: #{_description}"

      # sku
      _sku = "tiktokbox-#{details[:name].gsub(' ', '-').downcase}"
      if _sku.length > 64
        how_many = _sku.length - 64
        _sku = "tiktokbox-#{details[:name][0..how_many].gsub(' ', '-').downcase}"
      end
      p "_sku #{_sku}"

      # price
      _price = details[:price].gsub(/\D/, '').to_i
      p "_price #{_price}"

      # stock
      _stock = 10

      # details
      # if details[:details]
      #   _details = details[:details].split(';').join("</li><li>") 
      #   _details = "<ul><li>#{_details}</li></ul>"
      #   p "_details #{_details}"
      # end
      
      # _finishing = details[:finishing]
      # _color = details[:color]
      # _details = "<ul><li>Finishing: #{_finishing}</li><li>Color: #{_color}</li></ul>"

      # _materials = details[:materials].split("\r").join("</li><li>")
      # _materials = "<ul><li>#{_materials}</li></ul>"

      # _dimensions = details[:dimensions] if details[:dimensions]

      _weight = 3

      driver.find_element(:id, "save_butn").click

      wait = Selenium::WebDriver::Wait.new(:timeout => 3)
      wait.until do
        driver.find_elements(:xpath, "//*[@id='root-wrapper']/div/div[1]/div[2]/div/div[3]/div[3]").size > 0
      end


      # sleep(3)
      # open category
      driver.find_element(:xpath, "//*[@id='wk_bodymain']/li[1]/div/div/div[3]/span[1]").click
      sleep(2)

      driver.find_element(:xpath, "//input[@name='category[]'][@value='10']").click
      driver.find_element(:xpath, "//input[@name='category[]'][@value='110']").click

      driver.find_element(:id, "name").clear
      driver.find_element(:id, "name").send_keys "#{_name}"
      driver.find_element(:id, "description").clear
      driver.find_element(:id, "description").send_keys "#{_description}"
      driver.find_element(:id, "descriptions").clear
      driver.find_element(:id, "descriptions").send_keys "#{_description}"
      driver.find_element(:id, "sku").clear
      driver.find_element(:id, "sku").send_keys "#{_sku}"
      driver.find_element(:id, "price").clear
      driver.find_element(:id, "price").send_keys "#{_price}"
      driver.find_element(:id, "stock").clear
      driver.find_element(:id, "stock").send_keys "#{_stock}"
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "inventory_stock_availability")).select_by(:text, "In Stock")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "visibility")).select_by(:text, "Catalog, Search")
      Selenium::WebDriver::Support::Select.new(driver.find_element(:id, "tax_class_id")).select_by(:text, "None")
      driver.find_element(:id, "weight").clear
      driver.find_element(:id, "weight").send_keys "#{_weight}"
      # driver.find_element(:id, "details").send_keys "#{_details}" if _details
      # driver.find_element(:id, "dimensions").send_keys "#{_dimensions}" if _dimensions
      # driver.find_element(:id, "materials").send_keys "#{_materials}"
      driver.find_element(:id, "availability").clear
      driver.find_element(:id, "availability").send_keys "Produk ini ready stock dan siap dikirim."
      # driver.find_element(:id, "care_instructions").clear
      # driver.find_element(:id, "care_instructions").send_keys ''
      # driver.find_element(:id, "shipping_return").clear
      # driver.find_element(:id, "shipping_return").send_keys ''

      # regex = Regexp.new(".#{details[:sku].downcase.gsub(' ', '.')}.", true)
      index = 0
      # image_files.select { |x| regex.match x }.each do |file|
      (1..5).each do |id| 
        # byebug
        next if details["image_#{id}".to_sym].nil?
        Dir.glob(Rails.root.join(tiktokbox_working_dir, "images", id.to_s, details["image_#{id}".to_sym])).each do |file|
          p "image: #{file}"
          if index == 0
            driver.find_element(:name, "images[]").clear
            driver.find_element(:name, "images[]").send_keys(file)
          else
            driver.find_element(:link, "Add More image").click
            driver.find_element(:css, "#childDiv#{index+12} > input[name=\"images[]\"]").clear
            driver.find_element(:css, "#childDiv#{index+12} > input[name=\"images[]\"]").send_keys(file)
          end
          index += 1
        end
      end

      driver.find_element(:name, "defaultimage").click
      driver.find_element(:id, "meta_title").clear
      driver.find_element(:id, "meta_title").send_keys "#{_name}"
      driver.find_element(:id, "meta_keyword").clear
      driver.find_element(:id, "meta_keyword").send_keys "jam dinding"
      driver.find_element(:id, "meta_description").clear
      driver.find_element(:id, "meta_description").send_keys "Jual jam dinding dengan model variatif dan unik dari TikTokBox. Gratis ongkos kirim se-Jabodetabek."
      
      save_btn = driver.find_element(:id, "save_butn")
      driver.action.move_to(save_btn).perform
      driver.find_element(:id, "save_butn").click

      # # remember uploaded
      uploaded_products << product_id

      # refresh on every completed upload
      CSV.open(Rails.root.join('data', 'tiktokbox', 'uploaded_products.csv'), 'wb') do |csv|
        uploaded_products.map { |id| csv << [id] }
      end

      # break
    end

  end
end
